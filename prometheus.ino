#include "led.h"
#include "web.h"

#include <WiFi.h>

void setup() {
  Serial.begin(115200);

  setupLed();
  setupWifi();
}

#include <functional>
void procrastinate(std::function<void()> f, int time) {
  // TODO: This waiting mechanism does not handle when the clock wraps around
  int waitEnd = millis() + random(10, 10 + delay_time);

  f();

  while (millis() < waitEnd)
    delay(1);
}

void loopFunction() {
  flame();
  handleWebService();
}

void loop() {
  int d = random(10, 10 + delay_time);  
  procrastinate(loopFunction, d);
}
