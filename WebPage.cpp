#include "WebPage.h"

WebPage::WebPage(const char* const path, const int pathLength,     std::function<void(Client&)> printer,
                 std::function<void(const String &, const String &)> parser)
  : path(path), pathLength(pathLength), printer(printer), parser(parser) {
  // TODO static_assert that the path begins with a slash and not ends with slash
}

bool WebPage::isMyPath(const String &path) const {
  int l1 = pathLength; // this->path.length();
  int l2 = path.length();

  if ((l1 != l2) && (l1 + 1 != l2))
    return false;

  return path.indexOf(this->path) == 0;
}

void WebPage::parseArguments(const HttpHeaderHandler &header) {
  header.armArgumentIterator();
  for (auto arg = header.getNextArgument(); std::get<0>(arg) != ""; arg = header.getNextArgument()) {
    parser(std::get<0>(arg), std::get<1>(arg));
  }
}

void WebPage::print(Client &client) {
  // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
  // and a content-type so the client knows what's coming, then a blank line:
  client.println("HTTP/1.1 200 OK");
  client.println("Content-type:text/html");
  client.println("Connection: close");
  client.println();

  printer(client);
}
