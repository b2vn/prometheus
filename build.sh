#!/bin/bash

arduino-cli compile -v --warnings "all" \
		--build-path `pwd`/build \
		--build-cache-path `pwd`/build/cached \
		--fqbn esp32:esp32:esp32
