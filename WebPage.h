#pragma once

#include "HttpHeaderHandler.h"
#include <Client.h>
#include <functional>

class WebPage {
  protected:
    // const String& path;
    const char *const path;
    const int pathLength;

    std::function<void(Client&)> printer;
    std::function<void(const String &, const String &)> parser;

  public:
    WebPage(const char* const path, const int pathLength, std::function<void(Client&)> printer,
            std::function<void(const String &, const String &)> parser);

    bool isMyPath(const String &path) const;
    void parseArguments(const HttpHeaderHandler &header);
    void print(Client &client);
};
