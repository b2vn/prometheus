#include "led.h"
#include <Arduino.h>

// Led related
strand_t strand = {.rmtChannel = 0, .gpioNum = 26, .ledType = LED_SK6812W_V1, .brightLimit = 80, .numPixels = 300};
strand_t * STRANDS [] = { &strand };
// #define COUNT_OF(x) ((sizeof(x)/sizeof(0[x])) / ((size_t)(!(sizeof(x) % sizeof(0[x])))))
int STRANDCNT = 1; // COUNT_OF(STRANDS);

// Flame settings
uint32_t flameColour = 0xff9315; // 0xeb5b16;
// uint32_t glowColour = 0x205000;
uint8_t flicker = 170;
uint8_t delay_time = 100;
uint8_t brightness = 17;
uint8_t ledSkip = 1;

void setAllPortsAsOutput() {
  // Init unused outputs low to reduce noise
  pinMode(0, OUTPUT);
  pinMode(2, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(14, OUTPUT);
  pinMode(15, OUTPUT);
  pinMode(16, OUTPUT);
  pinMode(17, OUTPUT);
  pinMode(18, OUTPUT);
  pinMode(19, OUTPUT);
  pinMode(21, OUTPUT);
  pinMode(22, OUTPUT);
  pinMode(23, OUTPUT);

  pinMode(25, OUTPUT);
  pinMode(26, OUTPUT);
  pinMode(27, OUTPUT);

  // 32 and 33 does not work like the rest of the pins - for some reason  
  // pinMode(32, OUTPUT); 
  // pinMode(33, OUTPUT);
  pinMode(34, OUTPUT);
  pinMode(35, OUTPUT);  
}

void setupLed() {
  digitalLeds_initDriver();

  setAllPortsAsOutput();

  auto strand = STRANDS[0];
  for(int i=0; i<STRANDCNT; ++i, ++strand) {
    pinMode(strand->gpioNum, OUTPUT);
  }
    
  int rc = digitalLeds_addStrands(STRANDS, STRANDCNT);
  if (rc) {
    Serial.print("Init rc = ");
    Serial.println(rc);
  }

  if (digitalLeds_initDriver()) {
    Serial.println("Init FAILURE: halting");
    while (true) {};
  }
  digitalLeds_resetPixels(STRANDS, STRANDCNT);
}

uint8_t get_w(uint32_t colour) {
  return 0xff & (colour >> 24);
}

uint8_t get_r(uint32_t colour) {
  return 0xff & (colour >> 16);
}
uint8_t get_g(uint32_t colour) {
  return 0xff & (colour >> 8);
}
uint8_t get_b(uint32_t colour) {
  return 0xff & colour;
}

void flame() {
  strand_t * pStrand = STRANDS[0];

  int w = get_w(flameColour);
  int r = get_r(flameColour);
  int g = get_g(flameColour);
  int b = get_b(flameColour);

  float scale = brightness/100.0;

  for (int i = 0; i < pStrand->numPixels; i++) {
    // TODO: Flickering should be calculated as a delta between 'flame' and 'glow'. The flicker should then be a float 0..1 and be multiplied pre colour channel.

    if(i % ledSkip) {
      pStrand->pixels[i] = pixelFromRGBW(0, 0, 0, 0);
    }
    else {
      int f = random(0, flicker);
      int w1 = (w - f)*scale;
      int r1 = (r - f)*scale;
      int g1 = (g - f)*scale;
      int b1 = (b - f)*scale;
      if (w1 < 0) w1 = 0;
      if (r1 < 0) r1 = 0;
      if (g1 < 0) g1 = 0;
      if (b1 < 0) b1 = 0;
      pStrand->pixels[i] = pixelFromRGBW(r1, g1, b1, w1);
    }
  }

  digitalLeds_drawPixels(STRANDS, 1);
}
