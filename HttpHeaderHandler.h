#pragma once
#include <WString.h>
#include <tuple>

class HttpHeaderHandler {
  protected:
    bool onFirstLine = true;
    String firstLine;
    int currentLineLength = 0;

    mutable int startOfArgument, endOfArgumentName, endOfArgument;

    int getEndOfNextArgument(const String &str, const int start) const;
    int getEndOfArgumentName(const String &str, const int start) const;
    String getNameOfArgument(const String &str, const int start) const;
    String getValueOfArgument(const String &str, const int start) const;
    
  public:
    enum class Method {
      unknown, get, post
    };

    bool add(const char c);
    void clear();
    const String& getFirstLine() const;
    
    Method getMethod() const;

    String getPath() const;
    void armArgumentIterator() const;
    std::tuple<String, String> getNextArgument() const;
};
