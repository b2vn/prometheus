# Led flame

This project uses an ESP32 and a LED strip to show different flame effects. Adjusting the flame effects (colour, brightness, flickiring, etc) can be done via a web interface.

![](https://lh3.googleusercontent.com/--UJ2VWJK6abOW6v8I0_-EnJSPkxwXBhgo1J0Swuw2FioJMfxzv8lmRhhjkjI-Zf57uDOxlAPDckBC50E6tAAUbV9kMT_yON7VNom9pBTyAgVX_ykrePHrV9N9VviHiFNFmzoX_kSVx_H23wkH3ePZHb--qDaMq-gco4Rvdtt5ybK09Sk9HBdRbvsx4h5b9pEkhmN31mUR6_Oa_zGo6YdMKGUKl-zMj1b3eY5SyP9Tfnk84qail8zSS2qZw-1WXVVnsGMgz9gAgVAvCqhBl8cW6mOUQRrT28WAKWbipBw_HhkMBw0c4AFd9DBu6LormNSINk-wxYuZbF57yPxC2TF1i4nCMW_gw93nEIgiJ4kzghsYxSYPs4OdUFZyMXpv2Gp81pOScTMcWJpoedxC5M6HJ-xAymAspQ9JROVji-v_k5kfjL28M8dya9aEW_xrIsAq4UADI8zSwLTRyQQITMWnSRjAI9RXxU5mjljkZZB8ZAZ_aBRyWSqlzbYr5N1CM9kgCfIsri0H_E61YiSEQLMezHtITFYignw1NrJD1GFEo9NM46rQiWm2ja6-prFzPf48iTDyxc6-Aa5f31Lj2jTyW2QKLuYR-zVOcoNKWmPEDxXPCZh4XL8YXLiNL3tcl9kFqzjQM0neWxpb-KWFQCVs1xX8jEDwtIkit0SBMXjyrUqwSvKLNGG1Xc=w349-h262-k-no)

The current implementation uses a WS2812 LED strip, but the code utilices a library capable of controlling many other LED strips and can therefore easily be modified to spedific needs.

## Why Prometheus?
This is not just an odd name, inspired by a (quite bad) alien movie. In the Greek mythology, Prometheus was the god of fire. Long story cut short, Prometheus had a falling out with Zeus because Prometheus had created humans. Zeus might have been more foresighted than Prometheus, and he hated Prometheus' vermonts. Nevertheless, Prometheus quite liked mankind and offered us the fire as a present. 

I'm glad to have the opportunity to offer you Prometheus as an open-source project, so you can create the fire effects you want. 

Now that the conflict between Zeus and Prometheus has warn off, this present should be safe and fun to use - but do take care when you connect the components or sparks may fly again ;)

[Source](https://en.wikipedia.org/wiki/Prometheus)

## Usage

### First time
* Connect the components
* Power up
* The esp will act as AP, connect to 'ip address/setup'
* Select the SSID to connect to
* How do we find the IP the esp gets once it is connected to the real wifi?

### Controlling leds

Go to the web page

## Build

Before you can build the source, you need to change the access point settings in `ap_settings.cpp`.

### Dependencies

(Note that the dependencies are resolved in the dockerfile, but they might be necessary if you build without the docker)

* ESP32 Digital RGB LED Drivers v. 1.5.3 by Martin Falatic

### Docker
There is a docker file provided for the project, that creates a container containing arduino-cli and all dependencies required for building the project. It is recommended that this is used to build the project. However, if you insist on building the project on you PC, the dockerfile should still provide you all details about dependencies you should need.

```terminal
$ docker build . -tespcli_dev
```

```terminal
$ docker run -it --rm -v`pwd`:/root/prometheus espcli_dev
```

```terminal
root@146277737f34:/# cd /root/prometheus
root@146277737f34:~/prometheus# ./build.sh
```

### Program the device

I have not been able to figure out how to do this from docker yet, so I'm still using arduino ide for programming - I know it is really sad since all dependencies are wrapped nicely in the docker and arduino-cli... Holefully soon.


### Other tools

I admit, this is a mess - at least for now, but changing the web sites in the code is not very elegant. If changes to the websites are required, it is recommended to implement the changes in the html file (for testability), and copy the changed parts of the page into the code.

A few tool that can come handy in that situation:

* Use [minifier](https://www.willpeavy.com/tools/minifier/) to compress the web site. This will remove unnecessary white spaces. 
* Use [cpp_text](http://tomeko.net/online_tools/cpp_text_escape.php?lang=en) to turn the webpage into a c-string. This will escape what requires and substitude newlines, tabs, etc with the appropiate C character codes.


