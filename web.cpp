#include "web.h"
#include "HttpHeaderHandler.h"
#include "WebPage.h"
#include "led.h"
#include <WiFi.h>
#include <vector>

static const char scripts_js[] PROGMEM = "var wslide=document.getElementById(\"w\");var rslide=document.getElementById(\"r\");var gslide=document.getElementById(\"g\");var bslide=document.getElementById(\"b\");var brslide=document.getElementById(\"brs\");var fslide=document.getElementById(\"f\");var tslide=document.getElementById(\"t\");var sslide=document.getElementById(\"s\");$.ajaxSetup({timeput:1000});updateSite();var sending=false;function preset(w, r, g, b, br, f, t, s){wslide.value=w;rslide.value=r;gslide.value=g;bslide.value=b;/*brslide.value=br;*/fslide.value=f;tslide.value=t;/*sslide.value=s;*/update();}function toHex(v){hex=parseInt(v).toString(16);while (hex.length < 2)hex=\"0\" + hex;return hex;}function getColour(){return toHex(wslide.value) + toHex(rslide.value) + toHex(gslide.value)+ toHex(bslide.value);}function updateSite(){c=getColour();f=fslide.value;t=tslide.value;br=brslide.value;s=sslide.value;flame.innerHTML=\"#\" + c;flame.style.backgroundColor=\"#\"+c.slice(2);flicker.innerHTML=f;time.innerHTML=\"0-\" + t + \" ms\";skip.innerHTML=s;bright.innerHTML=br + \" %\";}function update(){moreToSend=true;updateSite();send();}var moreToSend=false;function send(){if(sending===true)return;sending=true;moreToSend=false;c=getColour();f=fslide.value;t=tslide.value;br=brslide.value;s=sslide.value;$.get(\"/?c=\"+c+\"&f=\"+f+\"&t=\"+t+\"&br=\"+br+\"&s=\"+s+\"&\");{Connection: close};setTimeout(function(){sending=false; if(moreToSend){send();}}, 250);}";
static const char control_html[] PROGMEM = "<html><head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"><style>body{text-align: center; font-family: \"Trebuchet MS\", Arial; margin-left:auto; margin-right:auto;}.slider{width: 300px;}</style><script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script><script src=\"loader.js\"></script> <script type=\"text/javascript\">window.onload=post_load; </script></head><body><h1>Led control</h1><input type=\"button\" value=\"Under water\" onclick=\"preset(0x00, 0x63, 0xbb, 0xf0, 40, 90, 150, 1);\"><input type=\"button\" value=\"Camp fire\" onclick=\"preset(0x00, 0xff, 0x73, 0x05, 27, 174, 100, 1);\"><input type=\"button\" value=\"Burning copper\" onclick=\"preset(0x00, 0xb6, 0xf3, 0x10, 40, 190, 70, 1);\"><input type=\"button\" value=\"Propane flame\" onclick=\"preset(0x00, 0x45, 0x59, 0xff, 40, 120, 60, 1);\"><h3>Custom</h3><p>Flame <span id=\"flame\"/></p>W: <input type=\"range\" min=0 max=255 class=\"slider\" oninput=\"update();\" id=\"w\"><br/>R: <input type=\"range\" min=0 max=255 class=\"slider\" oninput=\"update();\" id=\"r\"><br/>G: <input type=\"range\" min=0 max=255 class=\"slider\" oninput=\"update();\" id=\"g\"><br/>B: <input type=\"range\" min=0 max=255 class=\"slider\" oninput=\"update();\" id=\"b\"><br/><p>Flicker <span id=\"flicker\"/></p><input type=\"range\" min=0 max=255 class=\"slider\" oninput=\"update();\" id=\"f\"><br/><p>Time <span id=\"time\"/></p><input type=\"range\" min=0 max=200 class=\"slider\" oninput=\"update();\" id=\"t\"><br/><p>Brightness <span id=\"bright\"/></p><input type=\"range\" min=1 max=100 class=\"slider\" oninput=\"update();\" id=\"brs\" value=17><br/><p>Led skip <span id=\"skip\"/></p><input type=\"range\" min=1 max=10 class=\"slider\" oninput=\"update();\" id=\"s\" value=1><br/><script src=\"scripts.js\"></script></body></html>";

// Set web server port number to 80
WiFiServer server(80);

// Variable to store the HTTP request
HttpHeaderHandler header;

// Private interface
void parseControlRequest(const String &key, const String& value);
void sendSlider(Client &client, String id, int value, int max, int min);
void sendControl(Client &client);
void sendControlWebSite(Client &client);
void sendAboutWebSite(Client &client);
void sendLoaderScript(Client &client);
void sendScripts(Client &client);

void dummyPrinter(Client &) {}
void dummyParser(const String&, const String&) {}


WebPage _webpages[] = {
  WebPage ("/", 1, sendControlWebSite, parseControlRequest),
  WebPage ("/about", 6, sendAboutWebSite, dummyParser),
  WebPage ("/loader.js", 10, sendLoaderScript, dummyParser),
  WebPage ("/scripts.js", 11, sendScripts, dummyParser),
};
std::vector<WebPage> webPages(_webpages, _webpages+sizeof(_webpages)/sizeof(_webpages[0]));

void setupWifi() {
  // Connect to Wi-Fi network with SSID and password
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  // Print local IP address and start web server
  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  server.begin();
}

bool handleWebPage(Client &client, const HttpHeaderHandler &header) {
  const String path = header.getPath();
  
  for(auto page : webPages) {
    if (page.isMyPath(path)) {
      page.print(client);
      page.parseArguments(header);

      return true;
    }            
  }
  return false;
}

void handleWebService() {
  WiFiClient client = server.available();

  if (client) {
    Serial.println("New Client.");

    while (client.connected()) {
      if (client.available()) {
        if (header.add(client.read())) {

          if(!handleWebPage(client, header)) {
            Serial.println("Requested unknown page");
          }

          break;
        }
      }
    }

    header.clear();

    // Close the connection
    client.stop();
    Serial.println("Client disconnected.");
    Serial.println("");
  }
}

void parseControlRequest(const String &key, const String& value) {
  if (key == "c") {
    flameColour = (uint32_t)strtoul(value.c_str(), NULL, 16);
  }
  else {
    int v = (uint32_t)strtoul(value.c_str(), NULL, 10);

    if (key == "f")
      flicker = v;
    else if (key == "t")
      delay_time = v;
    else if (key == "br")
      brightness = v;
    else if (key == "s")
      ledSkip = v;
  }
}

void sendControlWebSite(Client &client) {
  client.print("<!DOCTYPE html>");
  client.print(control_html);
}

void sendAboutWebSite(Client &client) {
  // Web site
  client.print("<!DOCTYPE html><html>");

  /*
     TODO: Add more informtion: Link to main page, status for wifi connection
  */

  client.print("<body>");
  client.print("<h1>Led control</h1>");
  client.print("Build time: ");
  client.print(__DATE__);
  client.print(" ");
  client.print(__TIME__);
  client.print("<br/>");

  client.print("</html>");
}

void sendLoaderScript(Client &client) {
  client.print("function post_load() {");
  client.print("  preset(");

  client.print(get_w(flameColour));
  client.print(", ");
    
  client.print(get_r(flameColour));
  client.print(", ");
    
  client.print(get_g(flameColour));
  client.print(", ");
    
  client.print(get_b(flameColour));
  client.print(", ");

  client.print(brightness);
  client.print(", ");

  client.print(flicker);
  client.print(", ");

  client.print(delay_time);
  client.print(", ");

  client.print(ledSkip);
    
  client.print(");");
  client.print("}");
}

void sendScripts(Client &client) {
  client.print(scripts_js);
}
