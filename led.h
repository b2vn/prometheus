#pragma once
#include "esp32_digital_led_lib.h"

// extern strand_t strand;
extern strand_t *STRANDS[];
extern int STRANDCNT;

// Flame settings
extern uint32_t flameColour;
// extern uint32_t glowColour;
extern uint8_t flicker;
extern uint8_t delay_time;
extern uint8_t brightness;
extern uint8_t ledSkip;

void setupLed();
void flame();

uint8_t get_w(uint32_t colour);
uint8_t get_r(uint32_t colour);
uint8_t get_g(uint32_t colour);
uint8_t get_b(uint32_t colour);
