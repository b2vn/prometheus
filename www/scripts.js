var wslide = document.getElementById("w");
var rslide = document.getElementById("r");
var gslide = document.getElementById("g");
var bslide = document.getElementById("b");

var brslide = document.getElementById("brs");
var fslide = document.getElementById("f");

var tslide = document.getElementById("t");
var sslide = document.getElementById("s");

$.ajaxSetup({timeput:1000});
updateSite();

var sending = false;

function preset(w, r, g, b, br, f, t, s) {
	wslide.value = w;
	rslide.value = r;
	gslide.value = g;
	bslide.value = b;

	/*brslide.value = br;*/
	fslide.value = f;

	tslide.value = t;
	/*sslide.value = s;*/

	update();
}

function toHex(v) {
	hex = parseInt(v).toString(16);
	while (hex.length < 2)
		hex = "0" + hex;
	return hex;
}

function getColour() {
	return toHex(wslide.value) + toHex(rslide.value) + toHex(gslide.value)+ toHex(bslide.value);
}

function updateSite() {
	c = getColour();
	f = fslide.value;
	t = tslide.value;
	br = brslide.value;
	s = sslide.value;

	flame.innerHTML = "#" + c;
	flame.style.backgroundColor = "#"+c.slice(2);

	flicker.innerHTML = f;
	time.innerHTML = "0-" + t + " ms";
	skip.innerHTML = s;
	bright.innerHTML = br + " %";
}

function update() {
	moreToSend = true;
	updateSite();
	send();
}

var moreToSend = false;

function send() {
	if(sending === true)
		return;

	sending = true;
	moreToSend = false;

	c = getColour();
	f = fslide.value;
	t = tslide.value;
	br = brslide.value;
	s = sslide.value;

	$.get("/?c="+c+"&f="+f+"&t="+t+"&br="+br+"&s="+s+"&");
	{ Connection: close};

	setTimeout(function() {
			sending=false; 
			if(moreToSend) {
				send();
			}
		}, 250
	);
}