FROM ubuntu:18.04
MAINTAINER Nikolaj Rahbek, nikolaj@nr2.dk

RUN apt-get update -qy && apt-get install -qy curl nano python python-serial

###############################################################################
## Install arduino cli
ENV cli_link https://github.com/arduino/arduino-cli/releases/download/0.6.0/arduino-cli_0.6.0_Linux_64bit.tar.gz

RUN curl -L -o /tmp/arduino-cli.tar.gz $cli_link \
		&& mkdir -p /tmp/arduino-cli \
		&& tar zxvf /tmp/arduino-cli.tar.gz -C /tmp/arduino-cli \
		&& mv /tmp/arduino-cli/arduino-cli /usr/bin/

###############################################################################
## Setup the arduino environment
ENV urls https://dl.espressif.com/dl/package_esp32_index.json
RUN arduino-cli core update-index --additional-urls $urls \
	&& arduino-cli core install esp32:esp32 --additional-urls $urls \
	&& arduino-cli lib install "ESP32 Digital RGB LED Drivers"
