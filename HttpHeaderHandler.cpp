#include "HttpHeaderHandler.h"

int HttpHeaderHandler::getEndOfNextArgument(const String &str, const int start) const {
  endOfArgument = firstLine.indexOf("&", start);
  if (endOfArgument == -1)
    endOfArgument = firstLine.indexOf(" ", start);
  return endOfArgument;
}

int HttpHeaderHandler::getEndOfArgumentName(const String &str, const int start) const {
  endOfArgumentName = str.indexOf("=", start);
  return endOfArgumentName;
}

String HttpHeaderHandler::getNameOfArgument(const String &str, const int start) const {
  if (start < 0)
    return "";

  endOfArgumentName = getEndOfArgumentName(str, start);
  if (endOfArgumentName < 0)
    return "";

  return str.substring(start + 1, endOfArgumentName);
}

String HttpHeaderHandler::getValueOfArgument(const String &str, const int start) const {
  if (start < 0)
    return "";

  endOfArgument = getEndOfNextArgument(str, start + 1);
  if (endOfArgument < 0)
    return "";

  return str.substring(start + 1, endOfArgument);
}

bool HttpHeaderHandler::add(const char c) {
  if (onFirstLine)
    firstLine += c;

  if (c == '\r') {
    // Just ignore this
  }
  else if (c == '\n') {
    onFirstLine = false;

    if (currentLineLength == 0) {
      return true;
    }
    currentLineLength = 0;
  }
  else {
    ++currentLineLength;
  }

  return false;
}

void HttpHeaderHandler::clear() {
  firstLine = "";
  onFirstLine = true;
  currentLineLength = 0;
}

const String& HttpHeaderHandler::getFirstLine() const {
  return firstLine;
}

HttpHeaderHandler::Method HttpHeaderHandler::getMethod() const {
  if (strstr(firstLine.c_str(), "GET") != NULL)
    return HttpHeaderHandler::Method::get;
  if (strstr(firstLine.c_str(), "POST") != NULL)
    return HttpHeaderHandler::Method::post;
  return HttpHeaderHandler::Method::unknown;
}

String HttpHeaderHandler::getPath() const {
  // There are basically three optiond for how the request can look:
  // "GET /about/?b=120 HTTP/1.1"
  // "GET /about HTTP/1.1"
  // "GET / HTTP/1.1"

  int p1 = firstLine.indexOf(" ") + 1;
  int p2 = firstLine.indexOf("?", p1);
  if (p2 != -1) {
    // Arguments was passed to the website - the path is between the first
    // space and the question mark
    return firstLine.substring(p1, p2);
  }

  p2 = firstLine.indexOf(" ", p1); // Second space
  if (p2 != -1) {
    return firstLine.substring(p1, p2);
  }

  // No idea what the path is then...
  return "";
}

void HttpHeaderHandler::armArgumentIterator() const {
  startOfArgument = firstLine.indexOf("?");
}

std::tuple<String, String> HttpHeaderHandler::getNextArgument() const {
  String key, value;

  if (startOfArgument != -1) {
    key = getNameOfArgument(firstLine, startOfArgument);
    value = getValueOfArgument(firstLine, endOfArgumentName);
    startOfArgument = endOfArgument;
  }

  return std::make_tuple(key, value);
}
